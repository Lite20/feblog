var index;
var pageindex;
$(document).ready(function () {
    $.get("./posts/postdex.json", function (data) {
        // populate the index
        index = data;

        // grab the index of the page to show
        if (window.location.hash) {
            pageindex = index.indexOf(window.location.hash.replace('#', ''));
            if (pageindex == '-1') {
                // index error? set the buttons to the list end by default
                pageindex = index.length - 1;
                $.get("./posts/" + target + ".json", unwravelPost).fail(function (resp) {
                    alert("Uh oh! Something's broken! Page failed to load...");
                });
            }
        } else pageindex = data.length - 1;

        // load the page
        loadPage(pageindex);
    }).fail(function (resp) {
        alert("Uh oh! Something's broken! Page failed to load...");
    });
});

function loadPage(id) {
    var target = index[id];
    window.location = window.location.origin + "#" + target;
    $.get("./posts/" + target + ".json", unwravelPost).fail(function (resp) {
        alert("Uh oh! Something's broken! Page failed to load...");
    });
}

function unwravelPost(post_data) {
    // show last post button if there is one
    if (pageindex - 1 < 0) $(".post-last").hide();
    else $(".post-last").show();

    // show next post button if there is one
    if (pageindex + 1 == index.length) $(".post-next").hide();
    else $(".post-next").show();

    // set post data in html
    $("#post-title").html(post_data.title + '<span class="arrow"></span>');
    $("#post-date").html(formatDate(new Date(post_data.date)));
    $("#post-content").html(post_data.content);
    $("#post-card").show();

    // compute width of text
    var width = getWidth();

    // compute height of post
    var height = $("#post-title").height() + $(".card-content").height() + $(".card-action").height() + 12;

    // begin animation sequence to show post
    $("#post-card").animate({
        height: height + "px"
    }, 500, function () {
        $("#post-title").animate({
            width: width + "px"
        }, 1000, function () {
            $("#post-card").css("overflow", "visible");
            $("#post-card").height("100%");
        });
    });
}

function wravelPost(cb) {
    $("#post-title").animate({
        width: "0px"
    }, 1000, function () {
        $("#post-card").css("overflow", "hidden");
        $("#post-card").animate({
            height: "0px"
        }, 500, function () {
            $("#post-card").hide();
            cb();
        });
    });
}

const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
];

function formatDate(date) {
    var daykey = 'nd';
    if (date.getDate().toString().split('').pop() === '1') daykey = 'st';
    else if (date.getDate().toString().split('').pop() === '3') daykey = 'rd';
    return days[date.getDay()] + ' ' +
        months[date.getMonth()] + ' ' +
        date.getDate() + '<sup>' +
        daykey + '</sup>, ' +
        date.getFullYear() + ' @ ' +
        (date.getHours() - (12 % date.getHours())) + ':' +
        date.getMinutes() +
        ((date.getHours() > 12) ? 'pm' : 'am');
}

function getWidth() {
    $("#post-title").hide();
    $("#post-title").width("min-content");
    var width = $("#post-title").width();
    $("#post-title").width("0px");
    $("#post-title").show();
    return width + 50;
}

function last() {
    pageindex--;
    wravelPost(function () {
        loadPage(pageindex);
    });
}

function next() {
    pageindex++;
    wravelPost(function () {
        loadPage(pageindex);
    });
}
