$(document).ready(function () {
    var domblock = document.createElement('DIV');
    domblock.id = 'floatyContainer';
    $('header').append(domblock);
    populateCubes();
});

$(window).on('resize', populateCubes);

function populateCubes() {
    var floatyCube;
    $('#floatyContainer').empty();
    for (var i = 0; i < window.innerWidth; i += Math.max(50 * Math.random(), 10)) {
        floatyCube = document.createElement('DIV');
        floatyCube.className = 'floatycube';
        floatyCube.style.animationDelay = (5 * Math.random()) + 's';
        floatyCube.style.animationDuration = Math.max(7 * Math.random(), 1) + 's';
        floatyCube.style.left = i + "px";
        $('#floatyContainer').append(floatyCube);
    }
}
