// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();

function requestPerm() {
    messaging.requestPermission().then(function () {
        console.log('Notification permission granted.');
        getToken();
    }).catch(function (err) {
        console.log('Unable to get permission to notify.', err);
    });
}

function getToken() {
    messaging.getToken().then(function (currentToken) {
        if (currentToken) {
            var http = new XMLHttpRequest();
            var url = "https://mcfish.space/api/sub";
            var params = "token=" + currentToken;
            http.open("POST", url, true);
            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            http.onreadystatechange = function () { //Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    console.log(http.responseText);
                }
            };

            http.send(params);
        } else {
            requestPerm();
        }
    }).catch(function (err) {
        console.log('An error occurred while retrieving token. ', err);
    });
}

messaging.onTokenRefresh(getToken);

// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a sevice worker
//   `messaging.setBackgroundMessageHandler` handler.
messaging.onMessage(function (payload) {
    console.log("Message received. ", payload);
});

$(document).ready(function () {
    getToken();
});
